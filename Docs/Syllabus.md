(sec:sylabus)=
# Syllabus

## Course Information

- **Instructor(s):** Michael McNeil Forbes [`m.forbes+581@wsu.edu`](mailto:m.forbes+581@wsu.edu)
- **Course Assistants:** 
- **Office:** Webster 947F
- **Office Hours:** MWF, 1pm - 2pm, Spark 223 (after class) or by appointment
- **Course Homepage:** https://schedules.wsu.edu/List/Pullman/20223/Phys/581/01
- **Class Number:** 581
- **Title:** Phys 581: Physics 581: Physics Inspired Computational Techniques
- **Credits:** 3
- **Recommended Preparation**: 
- **Meeting Time and Location:** MWF, 12:10pm - 1pm, [Webster 941](https://sah-archipedia.org/buildings/WA-01-075-0008-13), Washington State University (WSU), Pullman, WA
- **Grading:** Grade based on assignments and project presentation.

```{contents}
```

## {ref}`sec:prerequisites`

```{toctree}
---
maxdepth: 2
---
Syllabus_Prerequisites
Syllabus_Assignments
```

There are no formal prerequisites for the course, but I will expect you to be
comfortable with the material discussed in the {ref}`sec:prerequisites` section, which
contains links to additional resources should you need to refresh your knowledge.
Please work with your classmates to try to share knowledge as needed.
Generally, I will expect familiarity with the following:

Domain Specific Preparation: 
: The most important prerequisite is the ability to communicate about and formulate
  complex problems in your field of study that would benefit from the techniques covered
  in this course.  Students will expected to actively engage with the techniques taught
  in this course, apply them to relevant problems in their domain of expertise, and to
  communicate about the efficacy to the class.

Linear Algebra
: Properties of Linear Operators (Self-Adjoint, Hermitian, Unitary,
  etc.), Matrix Factorization including the Singular Value Decomposition, Bases and
  Orthogonalization via
  [Gram-Schmidt](https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process).

Real and Complex Analysis
: Topology (notions of continuity), Calculus, [Banach
  spaces](https://en.wikipedia.org/wiki/Banach_space) (e.g. conditions for the existence
  of extrema), Fourier Analysis, Contour Integration, Conformal Maps.

Differential Equations
: Formulation of differential equations, existence of solutions and boundary value
  requirements, [Sturm-Liouville
  Theory](https://en.wikipedia.org/wiki/Sturm%E2%80%93Liouville_theory). 

Programming Skills
: There are some specific skills you will need for this course, including basic
  programming skills, distributed version control, how to connect remotely to computers
  etc. with [SSH][].  We will use the [CoCalc][] platform so you do not need to install any
  of the software on your computer.  See the {ref}`sec:prerequisites` section for details
  and learning resources.


### Textbooks and Resources

There is no formal textbook, but the following (available electronically from the WSU
Library) will be useful: 

{cite:p}`Gezerlis:2020`
: [A. Gezerlis: "Numerical Methods in Physics with Python" (2020)](
  https://ntserver1.wsulibs.wsu.edu:2532/core/books/numerical-methods-in-physics-with-python/563DF013576DCC535668A100B8F7D2F9). 
  This covers many useful algorithms with a focus on implementing them from scratch rather
  than using them as black-box routines.
  
{cite:p}`VanderPlas:2016`
: [J. VanderPlas: "Python Data Science Handbook" (2016)](
  https://ntserver1.wsulibs.wsu.edu:2171/lib/wsu/detail.action?docID=4746657&pq-origsite=primo).
  More focused on data analysis, but Jake provides a very good description of techniques
  like plotting and animation.  I have learned many of my skills from him and his blog
  [Pythonic Perambulations](https://jakevdp.github.io/).

Readings will be assigned as needed.  See {ref}`sec:References` for details and
additional resources.

### Student Learning Outcomes

The main objective of this course is for students to be able to effectively use
numerical techniques to solve complex problems following principles of good software
engineering and reproducible computing.

1. **Reproducible Computing:** Write and communicate computational results in a way that
   allows one to reproduce these results in the future, possibly on different computing
   platforms.
2. **Problem Assessment:** Be able to assess complex problems to determine if computational
   techniques will contribute to their solutions.
3. **Choice of Algorithm/Technique:** Be able to identify which algorithms are appropriate for
   solving the computational problems.
4. **Implementation:** Be able to implement the chosen algorithm.
5. **Convergence and Performance Analysis:** Be able to quantify and understand the
   convergence and performance of the algorithm being used.
6. **Solution Assessment:** Be able to assess the validity of the solution.  For
   example, to quantify the uncertainties associated with the results, accounting for
   the accumulation of round-off, truncation, and discretization errors, with an
   understanding of the conditioning of both the problem and the algorithm.
7. **Communicate and Defend the Solution:** Communicate the results with peers,
   defending the approximations made, the accuracy of the techniques used, and
   the assessment of the solutions.  Demonstrate insight into the nature of the
   solutions, making appropriate generalizations, and providing intuitive
   descriptions of the quantitative behavior found from solving the problem.

These learning outcomes will be assessed through the assignments as follows:

* By maintaining a code repository subject to automated testing, students will have to
    master all skills needed for reproducible computing (1).
* Through the successful completion of their assignments, students will demonstrate the
    ability to choose a technique (3) and implement it (4).
* Class assignments will test both accuracy and performance of the code.  Passing the
    associated tests will require that students have understood the convergence and
    performance properties of there code (5).
* Code coverage will ensure that students implement tests for their code, assessing
    their solutions (6).
* Through code reviews and documentation, students will be required to communicate and
  defend their solutions (7).

### Expectations for Student Effort

Students are expected to:

1. Stay up to date with reading assignments.
2. Participate in online forums, both asking questions and addressing peers questions.
3. Identify areas of weakness, work with peers to understand difficult
   concepts, then present remaining areas of difficulty to the instructor for
   personal attention or for discussion in class.
4. Complete assignments on time, providing well crafted solutions to the posed problems
   that demonstrate mastery of the material through the [Learning
   Outcomes](#learning-outcomes) 1-7.
   
   Student's are encouraged to discuss their intended approach with peers and with the
   instructor, but must ultimately write their own code.

For each hour of lecture equivalent, students should expect to have a minimum
of two hours of work outside class.

### Assessment and Grading Policy

Grading will be largely automated: you are expected to maintain a project on [GitLab]
where you keep the code in a [Git] clone of the [Official Course Repository].  Your
projects should contain all of the code you develop for this course, including solutions
to the assignments.  This code should be well-documented, and well-tested using
[GitLab]'s continuous integration (CI) tools.

Each assignment (see {ref}`sec:assignments` for details) will be distributed as a set of
files made available through the `main` branch of the [Official Course Repository]: you
will be instructed when to pull and merge these with your project.  These assignments
will contain skeleton code and tests.  You will be expected to complete the skeleton
code so that the tests pass.

These will include python code with skeleton functions, class, etc. which you are
expected to complete, along with automated tests (which you must not modify) for the
assignment, and CI instructions such that these tests can be run on [GitLab], producing
a badge for that assignment.

Your numeric grade for the course will be the percentage of passing assignment test
badges for your code.  Thus, if there are 10 assignments in the course and
you have passing tests for 8 of those, you will have a grade of 80%.

The final grade will be converted to a letter grade using the following scale: 

| Percentage P       | Grade |
| ------------------ | ----- |
| 90.0% ≤ P          | A     |
| 85.0% ≤ P \< 90.0% | A-    |
| 80.0% ≤ P \< 85.0% | B+    |
| 75.0% ≤ P \< 80.0% | B     |
| 70.0% ≤ P \< 75.0% | B-    |
| 65.0% ≤ P \< 70.0% | C+    |
| 60.0% ≤ P \< 65.0% | C     |
| 55.0% ≤ P \< 60.0% | C-    |
| 50.0% ≤ P \< 55.0% | D+    |
| 40.0% ≤ P \< 50.0% | D     |
| P \< 40.0%         | F     |

As discussed in section {ref}`sec:assignments`, there will be two stages of testing for each
assignment.  Completion of the first stage tests will allow you to get up to a grade of
A-.  Successful completion of sufficient second stages will be required for an overall A
in the course.  *Currently there is no other mechanism for partial credit, but I will
attempt to implement this during the course.*

As a fail-safe, you will get a minimum grade of a B+ if you meet the following
requirements:

* You make a reasonable attempt to solve the assignments by writing functioning code in
  your project.  This code should be well documented, appropriately commented, and
  should follow best coding practices.
* You write comprehensive automated tests for your code which pass when run using the
  [GitLab] CI, resulting in at least 85% code coverage.
* You participate in at least one formal code review of either code pertaining to a
  project in your domain of focus, or a randomly selected assignment, where we go
  over your code as a class, looking for potential bugs, places for optimizing
  performance, etc.

The `main` branch of the [Official Course Repository] will meet these requirements, and
will provide a skeleton of all the code needed to make and run the tests.  Thus, you should be
able to easily maintain this standard as you develop code, obtaining a minimum
grade of B+ with modest effort.  I expect some of the assignments will be challenging,
so to obtain a grade of A will require some commitment and skill.

There will be no exams in this course.

### Attendance and Make-up Policy 

While there is no strict attendance policy, students are expected attend an participate
in classroom activities and discussion. Students who miss class are expected to cover
the missed material on their own, e.g. by borrowing their classmates notes, reviewing
recorded lectures (if available), etc.

### Course Timeline
<!-- 16 Weeks -->

The following details the content of the course.  It essentially
follows the main textbook.  Content from the supplement will be
inserted as appropriate throughout. Details and further resources will
be included on the lecture pages on the [Canvas][] server.

1. Introduction and Basic Principles *(~2-3 week)*
    - Structure of the course.
    - Establish accounts and appropriate projects on [CoCalc][] and [GitLab][]
    - Numerical Evaluation of functions: Round-off error etc. (Assignment 0).
    - The Monty Hall Problem: Simple Monte Carlo Analysis (Assignment 1).
2. Basic Techniques *(~2-3 weeks)*
    - Differentiation and Integration.
    - Optimization and Root Finding.
    - Interpolation: Splines, Polynomials, Radial Basis Functions (RBF), Gaussian
      Processes.
    - Loops, Arrays, etc.
    - Bases.
3. Curve Fitting/Cycle Finding *(~2 week)*
   - Finding cycles in data.
   - Least squares fitting.
   - Fourier analysis.
   - Other techniques.
   - Bayesian techniques.
4. Ordinary Differential Equations (ODEs) *(~1-2 week)*
   - Orbiting Planets.
   - Falling Objects.
   - Agent-Based Modeling.
5. Partial Differential Equations (PDFs) *(~2 weeks)*
   - Schrodinger Equation
   - Fluid Dynamics
   - Diffusion
6. Other Topics/Domain Specific Problems *(~4-6 weeks)*
   - Renormalization Group, Effective Theories
   - Cellular Automata
   - Domain specific problems designed to address challenging problems in the fields of
       study represented by participants in the class.
   - Visualization techniques.
   - Profiling and Optimization.

### Topics
Exact topics and techniques will be chosen to match the interests and needs of the
participants (who are expected to communicate these needs to the instructor in advance
of the course), but may include:

* Case-studies: Part of the course will explore real-world examples of how physics has
  inspired solutions to hard problems in other fields of study.
  * [Renormalization Group Theory to understand traffic flow](https://discourse.iscimath.org/t/mar-17-renormalization-group-theory-the-systems-engineering-perspective/491).
* Root-finding and Optimization
  * Finding ground states and excited states in quantum mechanics.
  * Density Functional Theory (DFT)
  * Machine learning *(Probably not in the first year...)*
* Reaction/Diffusion networks:
  * Heat flow.
  * Hair follicle morphogenesis ([Turing's problem](https://en.wikipedia.org/wiki/The_Chemical_Basis_of_Morphogenesis)).
* Ordinary Differential equations and Boundary Value Problems:
  * Classical Mechanics
  * Classical Chaos
  * Time-dependent Schrödinger equation.
* Partial Differential Equations
  * Time-dependent Schrödinger equation/Time-dependent DFT (TDDFT)
  * Fluid dynamics *(Continuing from Matt's course)*.
    * Simple parallization: what to do when your data does not fit on a single node.  *(@m.duez)*
  * Numerical Relativity *(Probably not in the first year...)*
  * Finite element techniques + Green's Functions for wave dynamics *(Talk to Phil)*.
* Monte Carlo
  * Simulation of thermodynamic systems 
  * Numerical experiments with Renormalization Group
  * Quantum Monte Carlo (QMC)  *(Probably not in the first year...)*
* Bayesian Analysis
  * Linear and Non-linear model fitting which properly characterized errors.
  * Manifold reduction techniques.
* Data Analysis
  * How to access, store, and analyze data.
  * Clustering and searching in high dimensions. *(@gworthey)*


## Other Information

### Policy for the Use of Large Language Models (LLMs) or Generative AI in Physics Courses

The use of LLMs or Generative AI such as Chat-GPT is becoming prevalent, both in
education and in industry.  As such, we believe that it is important for students to
recognize the capabilities and inherent limitations of these tools, and use them
appropriately.

To this end, **please submit 4 examples of your own devising:**
* Two of which demonstrate the phenomena of "hallucination" -- Attempt to use the tool
  to learn something you know to be true, and catch it making plausible sounding
  falsehoods.
* Two of which demonstrate something useful (often the end of a process of debugging and
  correcting the AI).

Note: one can find plenty of examples online of both cases.  Use these to better
understand the capabilities and limitations of the AIs, but for your submission, please
find your own example using things you know to be true. *If you are in multiple courses,
you may submit the same four examples for each class, but are encouraged to tailor your
examples to the course.*

Being able to independently establish the veracity of information returned by a search,
an AI, or indeed any publication, is a critical skill for a scientist.  **If you are the
type of employee who can use tools like ChatGPT to write prose, code etc., but not
accurately validate the results, then you are exactly the type of employee that AI will
be able to replace.** 

Any use of Generative AI or similar tools for submitted work must include:
1. **A complete description of the tool.** (E.g. *"ChatGPT Version 3.5 via CoCalc's
   interface"* or *Chat-GPT 4 through Bing AI using the Edge browser"*, etc.)
2. **A complete record of the queries issued and response provided.**  (This should be
   provided as an attachment, appendices, or supplement.)
3. **An attribution statement consistent with the following:**
   *“The author generated this <text/code/etc.> in part with <GPT-3, OpenAI’s
   large-scale language-generation model/etc.> as documented in appendix <1>. Upon
   generating the draft response, the author reviewed, edited, and revised the response
   to their own liking and takes ultimate responsibility for the content.”*

<!-- ### COVID-19 Statement -->
<!-- Per the proclamation of Governor Inslee on August 18, 2021, **masks that cover both the -->
<!-- nose and mouth must be worn by all people over the age of five while indoors in public -->
<!-- spaces.**  This includes all WSU owned and operated facilities. The state-wide mask mandate -->
<!-- goes into effect on Monday, August 23, 2021, and will be effective until further -->
<!-- notice.  -->
 
<!-- Public health directives may be adjusted throughout the year to respond to the evolving -->
<!-- COVID-19 pandemic. Directives may include, but are not limited to, compliance with WSU’s -->
<!-- COVID-19 vaccination policy, wearing a cloth face covering, physically distancing, and -->
<!-- sanitizing common-use spaces.  All current COVID-19 related university policies and -->
<!-- public health directives are located at -->
<!-- [https://wsu.edu/covid-19/](https://wsu.edu/covid-19/).  Students who choose not to -->
<!-- comply with these directives may be required to leave the classroom; in egregious or -->
<!-- repetitive cases, student non-compliance may be referred to the Center for Community -->
<!-- Standards for action under the Standards of Conduct for Students. -->

### Academic Integrity

You are responsible for reading WSU’s [Academic Integrity Policy][], which is based on
[Washington State law][]. If you cheat in your work in this class you will: 

* Fail the course.
* Be reported to the [Center for Community Standards][].
* Have the right to appeal the instructor's decision.
* Not be able to drop the course or withdraw from the course until the appeals process
  is finished.

If you have any questions about what you can and cannot do in this course, ask your instructor.

If you want to ask for a change in the instructor's decision about academic integrity,
use the form at the [Center for Community Standards][] website. You must submit this
request within 21 calendar days of the decision.

[Academic Integrity Policy]: 
  <https://communitystandards.wsu.edu/policies-and-reporting/academic-integrity-policy/>
[Washington State law]: <https://apps.leg.wa.gov/wac/default.aspx?cite=504-26-202>
[Center for Community Standards]: <https://communitystandards.wsu.edu/>

<!-- Academic integrity is the cornerstone of higher education.  As such, all members of the -->
<!-- university community share responsibility for maintaining and promoting the principles -->
<!-- of integrity in all activities, including academic integrity and honest -->
<!-- scholarship. Academic integrity will be strongly enforced in this course.  Students who -->
<!-- violate WSU's Academic Integrity Policy (identified in Washington Administrative Code -->
<!-- (WAC) [WAC 504-26-010(4)][wac 504-26-010(4)] and -404) will fail the course, will not -->
<!-- have the option to withdraw from the course pending an appeal, and will be Graduate: -->
<!-- 6300, 26300 reported to the Office of Student Conduct. -->

<!-- Cheating includes, but is not limited to, plagiarism and unauthorized collaboration as -->
<!-- defined in the Standards of Conduct for Students, [WAC 504-26-010(4)][wac -->
<!-- 504-26-010(4)]. You need to read and understand all of the [definitions of -->
<!-- cheating][definitions of cheating].  If you have any questions about what is and is not -->
<!-- allowed in this course, you should ask course instructors before proceeding. -->

<!-- If you wish to appeal a faculty member's decision relating to academic integrity, please -->
<!-- use the form available at \_communitystandards.wsu.edu. Make sure you submit your appeal -->
<!-- within 21 calendar days of the faculty member's decision. -->

<!-- Academic dishonesty, including all forms of cheating, plagiarism, and fabrication, is -->
<!-- prohibited. Violations of the academic standards for the lecture or lab, or the -->
<!-- Washington Administrative Code on academic integrity -->

### University Syllabus
<!-- Required as of Fall 2023 -->

Students are responsible for reading and understanding all university-wide policies and
resources pertaining to all courses (for instance: accommodations, care resources,
policies on discrimination or harassment), which can be found in the [university
syllabus][].

[university syllabus]: <https://syllabus.wsu.edu/university-syllabus/>

### Students with Disabilities

Reasonable accommodations are available for students with a documented
disability. If you have a disability and need accommodations to fully
participate in this class, please either visit or call the Access
Center at (Washington Building 217, Phone: 509-335-3417, E-mail:
<mailto:Access.Center@wsu.edu>, URL: <https://accesscenter.wsu.edu>) to schedule
an appointment with an Access Advisor. All accommodations MUST be
approved through the Access Center. For more information contact a
Disability Specialist on your home campus.

### Campus Safety

Classroom and campus safety are of paramount importance at Washington
State University, and are the shared responsibility of the entire
campus population. WSU urges students to follow the “Alert, Assess,
Act,” protocol for all types of emergencies and the “[Run, Hide, Fight]”
response for an active shooter incident. Remain ALERT (through direct
observation or emergency notification), ASSESS your specific
situation, and ACT in the most appropriate way to assure your own
safety (and the safety of others if you are able).

Please sign up for emergency alerts on your account at MyWSU. For more
information on this subject, campus safety, and related topics, please
view the FBI’s [Run, Hide, Fight] video and visit [the WSU safety
portal][the wsu safety portal].

### Students in Crisis - Pullman Resources 

If you or someone you know is in immediate danger, DIAL 911 FIRST! 

* Student Care Network: https://studentcare.wsu.edu/
* Cougar Transit: 978 267-7233 
* WSU Counseling and Psychological Services (CAPS): 509 335-2159 
* Suicide Prevention Hotline: 800 273-8255 
* Crisis Text Line: Text HOME to 741741 
* WSU Police: 509 335-8548 
* Pullman Police (Non-Emergency): 509 332-2521 
* WSU Office of Civil Rights Compliance & Investigation: 509 335-8288 
* Alternatives to Violence on the Palouse: 877 334-2887 
* Pullman 24-Hour Crisis Line: 509 334-1133 

[communitystandards.wsu.edu]: https://communitystandards.wsu.edu/
[definitions of cheating]: https://apps.leg.wa.gov/WAC/default.aspx?cite=504-26-010
[run, hide, fight]: https://oem.wsu.edu/emergency-procedures/active-shooter/
[the wsu safety portal]: https://oem.wsu.edu/about-us/
[wac 504-26-010(4)]: https://apps.leg.wa.gov/WAC/default.aspx?cite=504-26
[SSH]: <https://en.wikipedia.org/wiki/Secure_Shell> "SSH on Wikipedia"
[Canvas]: <https://wsu.instructure.com/courses/1488567>
[CoCalc]: <https://cocalc.com> "CoCalc: Collaborative Calculation and Data Science"
[GitLab]: <https://gitlab.com> "GitLab"
[GitHub]: <https://github.com> "GitHub"
[Git]: <https://git-scm.com> "Git"
[Anki]: <https://apps.ankiweb.net/> "Powerful, intelligent flash cards."


[Official Course Repository]: <https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation> "Official Course Repository hosted on GitLab"
[Shared CoCalc Project]: <https://cocalc.com/74852aba-2484-4210-9cf0-e7902e5838f4/> "Shared CoCalc Project"
[WSU Courses CoCalc project]: <https://cocalc.com/projects/c31d20a3-b0af-4bf7-a951-aa93a64395f6>
