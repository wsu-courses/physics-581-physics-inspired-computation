<!-- Physics 581: Physics Inspired Computational Techniques documentation master file, created by
   sphinx-quickstart on Tue Aug 10 12:38:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->

<!-- Literally include the README.md file -->
```{include} README.md
```

<!-- Include ../README.md
     If you would like to use the contents of your top-level README.md file here, then
     you can literally include it here with the following:

```{include} ../README.md
``` 

    Note that this may will break `sphinx-autobuild` (`make doc-server`) which will not rebuild
    this index file when ../README.md changes.  See the note at the bottom of the file
    if you want to do this while using sphinx-autobuild.
--> 

# Phys 581 - Physics Inspired Computing

Welcome to Phys 581 - Physics Inspired Computing!  This is the main documentation page for the
course.  For more class information, please see the {ref}`sec:sylabus`.

This website, hosted on [Read The Docs](https://wsu-phys-581-computation.readthedocs.io/en/latest), will be used to
collect and display additional information about the course, including:
* {ref}`sec:sylabus`
* {ref}`sec:assignments`
* {ref}`sec:readings`

and various class notes.  These should also be available through the navigation menu
(which might hidden if your display is not sufficiently wide).

These documents are built using [JupyterBook]() (see {ref}`sec:demonstration`) and
include all of the source code needed to generate the figure, plots etc.  For example,
to see how a figure was made, look in the preceding code cell.
The complete source code for this documentation is available at
<https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation>. 

## Funding Statement
<a href="https://www.nsf.gov"><img width="10%"
src="https://nsf.widen.net/content/txvhzmsofh/png/" />
</a>
<br>

Some of the material presented here is based upon work supported by the National Science
Foundation under [Grant Number 2012190](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2012190). Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the author(s) and do not
necessarily reflect the views of the National Science Foundation.
 



Instructors: the information presented here at the start of the course documentation is
contained in the `Docs/index.md` file, which you should edit to provide an overview of
the course.

One reasonable option might be to replace this by a literal include of the top-level
`README.md` file with the following code:

````markdown
```{include} ../README.md
``` 
````

<!-- We use a hack here to get a relative link in the TOC.  
     This will fail with LaTeX etc.
     https://stackoverflow.com/a/31820846/1088938 
     https://github.com/sphinx-doc/sphinx/issues/701 -->
```{toctree}
---
maxdepth: 2
caption: "Contents:"
titlesonly:
hidden:
---
GettingStarted
Syllabus
Syllabus_Prerequisites
References
Assignments
api/index
```
```{toctree}
---
maxdepth: 2
caption: "Prerequisites:"
titlesonly:
hidden:
glob:
---
Prerequisites/*
```


```{toctree}
---
maxdepth: 2
caption: "Other Content:"
hidden:
---
Notes
Projects
InstructorNotes
Demonstration
CoCalc
PythonPackaging
ClassLog
```

<!-- This includes another README.md rendering that will get updated if the file is
     changed so that we can see updates when using make doc-sever.  The reason is that,
     because we generate the main page using a literal include ({include} README.md...),
     the main page will only get updated if we change this index.md file.
     
     We do not include this extra link when we build on RTD.  We do this using the
     sphinx.ext.ifconfig extension:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd

   .. toctree::
      :maxdepth: 0
      :caption: "Includes (for autobuild):"
      :titlesonly:
      :hidden:

      README
   
```
-->

[JupyterBook]: <https://jupyterbook.org>
