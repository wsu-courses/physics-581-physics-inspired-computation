(sec:readings)=
Resources, Readings, and References
===================================

## Textbook

The course will make regular reference to {cite:p}`Gezerlis:2020`.

## Bayesian Analysis

For a serious introduction to Bayesian Analysis, read sections I through III.A of
{cite:p}`Toussaint:2011`.

## Gaussian Processes

* {cite:p}`Gelman:2013`: A good comprehensive reference.  Available for non-commercial
  from the [author's webpage](https://www.stat.columbia.edu/~gelman/book/).
* {cite:p}`Melendez:2019`: An application to nuclear theory with associated code [`gsum`].
* {cite:p}`Rasmussen:2006`: Another nice book that formed the basis for the
  implementations in [scikit-learn].

## Matrix Cookbook

The Matrix Cookbook {cite:p}`Petersen:2012` contains many useful formulae for working with matrices (without
proof).  This version is rather old, so the following updates may also be useful:

* [Matrix Forensics](https://github.com/r-barnes/MatrixForensics): An open-source
  repository with a compendium of formulae.
* [The Matrix Reference Manual](http://www.ee.ic.ac.uk/hp/staff/dmb/matrix/intro.html).

## Tips

```{toctree}
---
maxdepth: 1
glob:
---
Tips/*
```

(sec:references)=
References
==========

```{bibliography}
:style: alpha
```

(sec:linear_algebra_resources)=
## Linear Algebra

* [Essence of linear algebra][]: A great set of highly visual videos by [3Blue1Brown][]
  getting you up to abstract vector spaces.
* [MIT 18.06 Linear Algebra]: A set of [video
  lectures](https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/video_galleries/video-lectures/)
  and accompanying material for the MIT Linear Algebra course.
* [Qiskit Linear Algebra](https://qiskit.org/textbook/ch-appendix/linear_algebra.html):
  A short introduction that is part of the [Qiskit][] platform.
* Appendix A of {cite:p}`Mermin:2007` has a nice short review of Dirac notation.


[MIT 18.06 Linear Algebra]: <https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/>
[3Blue1Brown]: <https://www.youtube.com/c/3blue1brown>
[Essence of linear algebra]: <https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab>
[Qiskit]: https://qiskit.org
